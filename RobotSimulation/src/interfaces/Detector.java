package interfaces;

public interface Detector {
	public boolean detects(Detectable d);
}
