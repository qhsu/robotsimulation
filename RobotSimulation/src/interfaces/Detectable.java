package interfaces;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;

public interface Detectable {
	public Rectangle getBounds();
	public Point getLocation();
}
