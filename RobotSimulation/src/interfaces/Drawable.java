package interfaces;

import java.awt.Graphics;
import java.awt.Rectangle;

/**
 * Objects who implement this interface will be responsible for drawing themselves using a Graphics object
 * 
 * @author Quinn
 *
 */

public interface Drawable {
	public void draw(Graphics g);
	public Rectangle getBounds();
}
