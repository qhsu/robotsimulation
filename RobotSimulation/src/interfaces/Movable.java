package interfaces;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;


/**
 * Objects which implement this interface will be responsible for moving themselves
 * @author Quinn
 *
 */

public interface Movable {
	public void setLocation(Point location);
	
	public Point getLocation();
	
	/**
	 * Move to a specific point specified by p
	 * @param location A point to be moved to
	 * @return The point moved to
	 */
	public void moveTo(Point location);
	
	/**
	 * Move by a specific delta specified by p
	 * @param location Specifies dx and dy values to move by
	 * @return The point moved to
	 */
	public void moveBy(Point location);
	
	/**
	 * Returns how fast the object can move
	 * @return the speed at which the object moves
	 */
	
	public void setSpeed(int speed);
	public int getSpeed();
	
	public void setDimension(Dimension dimension);
	public Dimension getDimension();
}
