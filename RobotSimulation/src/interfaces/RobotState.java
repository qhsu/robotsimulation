package interfaces;

import exceptions.RobotStateError;
import objects.Robot;

public interface RobotState {
	public void execute() throws RobotStateError; 
}
