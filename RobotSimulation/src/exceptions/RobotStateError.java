package exceptions;

@SuppressWarnings("serial")
public class RobotStateError extends Error {
	public RobotStateError(String s) { super(s); }
}
