package states;

import java.awt.Rectangle;

import interfaces.RobotState;
import objects.Robot;

public class StateManager implements RobotState {
	protected Robot r; 
	protected Rectangle worldBounds;
	protected RobotState moveForward;
	
	protected RobotState currentState = null;
	
	public StateManager(Robot r, Rectangle worldBounds) {
		this.r = r;
		this.worldBounds = worldBounds;
		moveForward = new MoveForwardState(r,worldBounds);
		
		currentState = moveForward;
	}
	
	public void execute() { currentState.execute(); }
}
