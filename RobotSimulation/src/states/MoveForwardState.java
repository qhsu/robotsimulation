package states;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;

import abstracts.BasicObject;
import exceptions.RobotStateError;
import interfaces.RobotState;
import objects.Robot;

public class MoveForwardState implements RobotState {
	/*
	 * This class represents the robot's behaviour of moving forward
	 */
	
	protected Rectangle worldBounds;
	protected Robot r;
	
	public MoveForwardState(Robot r, Rectangle worldBounds) {
		this.r = r;
		this.worldBounds = worldBounds;
	}

	@Override
	public void execute() throws RobotStateError {
		r.moveBy(new Point(r.getSpeed(),0));
		Shape mBounds = r.getShape();
		if (!worldBounds.contains(mBounds.getBounds())) {
			r.moveBy(new Point(-r.getSpeed(),0));
			throw new RobotStateError("Moving forward would move past the world bounds");
		}
		
	}
}
