package abstracts;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;

import interfaces.Scheduled;

/**
 * A basic object in the environment that should be drawable and movable
 * @author Quinn
 *
 */

public class BasicObject extends DrawableObject implements Scheduled {
	
	protected String description;
	
	public BasicObject() { }
	
	public String getDescription() { return this.description; }
	public void setDescription(String description) { this.description = description; }
	
	// there is no setShape method, as we rely on the super class to provide that
	// however, subclasses of this class should override the setDimension method
	// such that when the dimensions are know, a default shape should be created
	// hence, the pattern should be
	//
	// public void setDimension(Dimension d) {
	//	super.setDimension(d);
	//  super.setShape(generateShapeHere());
	// }
	
	@Override
	public void tick() {
		moveBy(new Point(10,0));
	}
	
	public Shape getShape() { 
		AffineTransform rotate = AffineTransform.getRotateInstance(0);
		AffineTransform translate = AffineTransform.getTranslateInstance(getLocation().getX(), getLocation().getY());
		
		// perform the transform with respect to the center of the shape, instead of the top left of the shape
		Shape s = translate.createTransformedShape(rotate.createTransformedShape(super.getShape()));
		return s; 
	}
	
	public void draw(Graphics g) {
		
		//super.draw(g);
		
		Graphics2D g2d = (Graphics2D)g;
		g2d.fill(getShape());
		
		g.setColor(Color.yellow);
		// the label should at least be inside the bounds of the box
		drawCenteredString(g, getDescription(), getShape().getBounds(), Font.decode("Arial-BOLD-12"));
		
		
		// Test the bounding box of the transformed shape
		g.setColor(Color.black);
		g2d.draw(getShape().getBounds2D());
	}
	
	public Rectangle getBounds() {
		return getShape().getBounds();
	}
	
	/**
	 * Draw a String centered in the middle of a Rectangle.
	 *
	 * @param g The Graphics instance.
	 * @param text The String to draw.
	 * @param rect The Rectangle to center the text in.
	 * 
	 * Code imported from: https://stackoverflow.com/questions/27706197/how-can-i-center-graphics-drawstring-in-java
	 */
	protected void drawCenteredString(Graphics g, String text, Rectangle rect, Font font) {
	    // Get the FontMetrics
	    FontMetrics metrics = g.getFontMetrics(font);
	    // Determine the X coordinate for the text
	    int x = rect.x + (rect.width - metrics.stringWidth(text)) / 2;
	    // Determine the Y coordinate for the text (note we add the ascent, as in java 2d 0 is top of the screen)
	    int y = rect.y + ((rect.height - metrics.getHeight()) / 2) + metrics.getAscent();
	    // Set the font
	    g.setFont(font);
	    // Draw the String
	    g.drawString(text, x, y);
	}
}
