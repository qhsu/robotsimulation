package abstracts;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;

import interfaces.Drawable;

public class DrawableObject extends MovableObject implements Drawable {
	protected Shape shape;
	
	public DrawableObject() { }
	
	// generally, all subclasses should be providing a default shape for free
	// this method is only here in the case that we want to change the shape of an already
	// instantiated object at runtime
	// i'm not sure what that case would be, but it is here for that purpose; however, all
	// subclasses are using this as their main setter function
	public void setShape(Shape shape) { this.shape = shape; }
	
	public Shape getShape() { return this.shape; }
	
	public Rectangle getBounds() { return this.shape.getBounds(); }
	
	@Override
	public void draw(Graphics g) {
		Graphics2D g2d = (Graphics2D)g;
		g2d.fill(getShape());	
	}

}
