package abstracts;

import java.awt.Dimension;
import java.awt.Point;

import interfaces.Movable;

public class MovableObject implements Movable {
	protected Point location;
	protected Dimension dimension;
	protected int speed = 10;
	
	public MovableObject() { }
	
	public void setLocation(Point location) { this.location = location; }
	
	public Point getLocation() { return location; }
	
	@Override
	public void moveTo(Point p) { location.setLocation(p.x, p.y); }

	@Override
	public void moveBy(Point p) { location.translate(p.x, p.y); }

	@Override
	public int getSpeed() { return this.speed; }

	@Override
	public void setSpeed(int speed) { this.speed = speed; }
	
	@Override
	public void setDimension(Dimension dimension) { this.dimension = dimension; }
	
	@Override
	public Dimension getDimension() { return this.dimension; }
	
	
}
