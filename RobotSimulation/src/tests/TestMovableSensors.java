package tests;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.awt.Dimension;
import java.awt.Point;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import factories.RobotFactory;
import objects.RobotWithSensors;
import objects.Sensor;

class TestMovableSensors {
	private RobotWithSensors r = RobotFactory.getInstance().buildBasicRobot("r", new Point(100,100), new Dimension(25,25), 30);
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testMoveBy() {
		// from the robot instantiation numbers above
		int x = 100+25;
		int y = 100;
		
		// make sure that the sensors are at the original point
		List<Sensor> sensors = r.getSensors().getSensors();
		Point origin = new Point(x,y);
		for(Sensor s: sensors) {
			assertTrue(s.getLocation().equals(origin));
		}
		
		Point moved = ((Point)origin.clone());
		moved.translate(10, 0);
		
		r.tick();
		
		assertTrue(r.getLocation().equals(new Point(110,100)));
		
		for(Sensor s: sensors) {
			assertTrue(s.getLocation().equals(moved));
		}
	}

}
