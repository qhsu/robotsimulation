package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Dimension;
import java.awt.Point;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import factories.BoxFactory;
import factories.RobotFactory;
import objects.Box;
import objects.Robot;

/**
 * Tests to see if the sensors will correct detect boxes with the built in java methods
 * However, I have a sneaking suspicion that these detection methods will not work and will give false-positives
 * when the boxes are rotated and the bounding boxes are much larger than the actual boxes.
 * @author Quinn
 *
 */

class TestDetectable {
	private Box b = BoxFactory.getInstance().buildDefaultBox("Box", new Point(130,100), new Dimension(200,100));
	private Robot r2 = RobotFactory.getInstance().buildBasicRobot("r2", new Point(100,100), new Dimension(25,25), 30);
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testBoxSurroundsRobot() {
		assertTrue(r2.detects(b));
		
	}
	
	@Test
	void testBoxTouchesSensor() {
		// just the tip of the center sensor touches the bound of the box
		// in this scenario, the box is not detected
		b.moveTo(new Point(255,100));
		assertFalse(r2.detects(b));
		
		// however, move the box one pixel left, and the box is detected
		// again
		b.moveBy(new Point(-1,0));
		assertTrue(r2.detects(b));
	}
	
	@Test
	void testBoxDoesNotTouchSensor() {
		b.moveTo(new Point(1000,100));
		assertFalse(r2.detects(b));
	}

}
