package tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import abstracts.BasicObject;
import factories.BoxFactory;
import objects.Box;

class TestBasicObject {
	/**
	 * This static class is only useful to test my abstract class since abstract classes cannot be instantiated.
	 * @author Quinn
	 *
	 */
	
	private static BasicObject o;
	private static Box b;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		o = new BasicObject();
		o.setDescription("basic object");
		o.setLocation(new Point(1,2));
		o.setDimension(new Dimension(100,200));
		
		b = BoxFactory.getInstance().buildDefaultBox("basic object", new Point(1,2), new Dimension(100,200));
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testDescription() {
		assertEquals("basic object", b.getDescription());
	}
	
	@Test
	void testLocation() {
		assertEquals(new Point(1,2), b.getLocation());
	}
	
	@Test
	void testDimensions() {
		assertEquals(new Dimension(100,200), b.getDimension());
	}
	
	@Test
	void testMoveTo() {
		b.moveTo(new Point(7,8));
		assertEquals(new Point(7,8), b.getLocation() );
	}

	@Test
	void testMoveBy() {
		b.moveBy(new Point(10,10));
		assertEquals(new Point(11,12), b.getLocation());
	}
	
	@Test
	void testBounds() {
		// make sure the bounding box is where we think it is, with the position at the center of the box
		// and not the top right
		Rectangle r = new Rectangle(1-50,2-100,100,200);
		assertEquals(b.getShape().getBounds(), r);
	}
}
