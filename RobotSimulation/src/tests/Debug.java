package tests;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;

import javax.swing.JFrame;
import javax.swing.JPanel;
import factories.RobotFactory;
import objects.Robot;

public class Debug {
	public static void drawRobot(Robot r) {
		@SuppressWarnings("serial")
		class Panel extends JPanel {
			public Dimension getPreferredSize() { return new Dimension(800, 600); } 
			public void paintComponent(Graphics g) {
				r.draw(g);
				Graphics2D g2d = (Graphics2D)g;
				g2d.draw(r.getShape().getBounds());
			}
		}
		
		JFrame f = new JFrame("Tester Frame");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
		f.add(new Panel(), BorderLayout.CENTER);
        f.pack();
        
        f.setVisible(true);
				
	}
	
	public static void main(String[] args) {
		Robot r = RobotFactory.getInstance().buildBasicRobot("r1", new Point(790, 100), new Dimension(10,10), 10);
		drawRobot(r);
	}
}
