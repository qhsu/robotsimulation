package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import exceptions.RobotStateError;
import factories.SensorArrayFactory;
import factories.StateManagerFactory;
import objects.RobotWithStateManager;

class TestRobotWithStateManager {
	RobotWithStateManager r;
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		// this REALLY needs to be in a factory
		// got a null pointer reference because I forgot to set sensors on the robot
		r = new RobotWithStateManager();
		r.setDescription("r1");
		r.setLocation(new Point(100,100));
		r.setDimension(new Dimension(10,10));
		r.setSensors(SensorArrayFactory.getInstance().buildDefaultSensorArray(new Point(100+10,100), 30));
		r.setStateManager(StateManagerFactory.getInstance().buildStateManager(r, new Rectangle(800,600)));
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testGetSpeed() { 
		assertEquals(10, r.getSpeed()); 
	}
	
	@Test
	void testTick() {
		r.tick();
		assertEquals(new Point(110, 100), r.getLocation());
	}
	
	@Test
	void testTickOutOfBounds() {
		// move to just where the final shape would be one pixel out of bounds of the world
		// ie. 781+10(shape size)+10(move speed)=801 pixels in x direction
		r.moveTo(new Point(781,100));
		assertThrows(RobotStateError.class, () -> {r.tick();});
	}

}
