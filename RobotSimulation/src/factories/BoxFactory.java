package factories;

import java.awt.Dimension;
import java.awt.Point;

import objects.Box;

public class BoxFactory {
	private static BoxFactory instance = null;
	
	private BoxFactory() {}
	
	public static BoxFactory getInstance() {
		if (instance == null) { instance = new BoxFactory(); }
		return instance;
	}
	
	public Box buildDefaultBox(String s, Point location, Dimension d) {
		Box b = new Box();
		b.setDescription(s);
		b.setLocation(location);
		b.setDimension(d);
		return b;
	
	}
}
