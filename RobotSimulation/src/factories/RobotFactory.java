package factories;

import java.awt.Dimension;
import java.awt.Point;

import objects.RobotWithSensors;
import objects.SensorArray;

/**
 * This will be the main factory class that will be used to create robots
 * @author Quinn
 *
 */

public class RobotFactory {
	private static RobotFactory instance;
	
	private RobotFactory() {
	}
	
	public static RobotFactory getInstance() {
		if (instance == null) { instance = new RobotFactory(); }
		return instance;
	}
	
	public RobotWithSensors buildBasicRobot(String s, Point location, Dimension size, int sensorLength) {
		SensorArray sensors = SensorArrayFactory.getInstance().buildDefaultSensorArray(new Point(location.x+size.width, location.y), sensorLength);
		RobotWithSensors robot = new RobotWithSensors();
		robot.setDescription(s);
		robot.setDimension(size);
		robot.setLocation(location);
		robot.setSensors(sensors);
		return robot;
	}
}
