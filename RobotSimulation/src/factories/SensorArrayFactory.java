package factories;

import java.awt.Point;

import objects.Sensor;
import objects.SensorArray;

public class SensorArrayFactory {
	private static SensorArrayFactory instance = null;
	
	private SensorArrayFactory() {}
	
	public static SensorArrayFactory getInstance() {
		if(instance == null) { instance = new SensorArrayFactory(); }
		return instance;
	}
	
	public SensorArray buildDefaultSensorArray(Point p, int sensorLength) {
		SensorArray sensors = new SensorArray();
		sensors.add(new Sensor(new Point(p.x, p.y), sensorLength, 0));
		sensors.add(new Sensor(new Point(p.x, p.y), sensorLength, Math.PI/6));
		sensors.add(new Sensor(new Point(p.x, p.y), sensorLength, -Math.PI/6));
		return sensors;
	}
}
