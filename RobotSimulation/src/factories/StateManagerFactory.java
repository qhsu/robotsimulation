package factories;

import java.awt.Rectangle;

import objects.Robot;
import states.StateManager;

public class StateManagerFactory {
	private static StateManagerFactory instance = null;
	
	public static StateManagerFactory getInstance() {
		if (instance == null) {
			instance = new StateManagerFactory();
		}
		return instance;
	}
	public StateManager buildStateManager(Robot r, Rectangle worldBounds) {
		return new StateManager(r, worldBounds);
	}
}
