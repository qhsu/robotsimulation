package objects;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import interfaces.Detectable;
import interfaces.Detector;
import interfaces.Drawable;
import interfaces.Movable;

/**
 * An array of sensors that the robot has.  
 * @author Quinn
 */

// converted draw, detects, moveBy and moveTo to use the new Java stream api
// requires Java 1.8 

public class SensorArray implements Drawable, Detector, Movable {
	ArrayList<Sensor> sensors;
	
	public SensorArray() {
		sensors = new ArrayList<Sensor>();
	}
	
	public ArrayList<Sensor> add(Sensor s) {
		sensors.add(s);
		return sensors;
	}

	@Override
	public void draw(Graphics g) { sensors.stream().forEach(s -> s.draw(g)); }

	@Override
	public boolean detects(Detectable d) {
		return sensors.stream().map(s->s.detects(d)).reduce(Boolean::logicalOr).get();
	}

	@Override
	public void moveTo(Point p) {
		sensors.stream().forEach(s -> s.moveTo(p));
	}

	@Override
	public void moveBy(Point p) {
		sensors.stream().forEach(s -> s.moveBy(p));
	}
	
	public List<Sensor> getSensors() { return sensors; }

	@Override
	public int getSpeed() {
		return sensors.get(0).getSpeed();
	}

	@Override
	public void setLocation(Point location) {
		sensors.stream().forEach(s -> s.setLocation(location));
		
	}

	@Override
	public Point getLocation() {
		return sensors.get(0).getLocation();
	}

	@Override
	public void setSpeed(int speed) {
		sensors.stream().forEach(s -> s.setSpeed(speed));
		
	}

	@Override
	// while this must be overriden, it doesn't make sense in the context of this object
	// as it seems unnecessary to change the dimensions of every sensor :S
	public void setDimension(Dimension dimension) { }

	@Override
	// this method, too, seems unnecessary to provide an implementation for
	public Dimension getDimension() { return null; }

	@Override
	public Rectangle getBounds() {
		return sensors.stream().map(s -> s.getBounds()).reduce((a,b) -> a.union(b)).get();
	}
}
