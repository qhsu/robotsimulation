package objects;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;

import interfaces.Detectable;

public class RobotWithSensors extends Robot {
	private SensorArray sensors;
	
	public void setSensors(SensorArray sensors) { this.sensors = sensors; }
	public SensorArray getSensors() { return sensors; }
	
	@Override
	public void draw(Graphics g) {
		super.draw(g);
		sensors.draw(g);
	}
	
	@Override
	public void moveBy(Point p) {
		sensors.moveBy(p);
		super.moveBy(p);
	}
	
	
	@Override
	public boolean detects(Detectable d) {
		return sensors.detects(d);
	}
	

}
