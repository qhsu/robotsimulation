package objects;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Polygon;

import abstracts.BasicObject;
import interfaces.Detectable;
import interfaces.Detector;
import interfaces.Scheduled;

public class Robot extends BasicObject implements Detector {
	
	public void setDimension(Dimension d) {		
		super.setDimension(d);
		super.setShape(calculateShape(d.getWidth()));
	}
	
	private static Polygon calculateShape(double radius) {
		
		// the base shape of the robot is a regular pentagon
		// only the width dimension of the parameter passed will be used to determine the radius of the circle
		// that circumscribes the pentagon
		
		int npoints=5;
		int[] xcoor = new int[5];
		int[] ycoor = new int[5];
		for(int i=0; i<npoints; i++) {
			// calculate the angle of the point
			double angle = i*(2*Math.PI)/5;
			xcoor[i] = (int)(radius*Math.cos(angle));
			ycoor[i] = (int)(radius*Math.sin(angle));
		}
		
		return new Polygon(xcoor, ycoor, npoints);
	}

	@Override
	public boolean detects(Detectable d) {
		return false;
	}

}
