package objects;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import abstracts.BasicObject;
import interfaces.Detectable;

public class Box extends BasicObject implements Detectable {
	public Box() { }
	
	
	public void setDimension(Dimension d) {
		super.setDimension(d);
		// we should always get a base shape for free
		super.setShape(new Rectangle(new Point(-d.width/2,-d.height/2), d));
	}
}
