package objects;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

import factories.StateManagerFactory;
import states.StateManager;

public class RobotWithStateManager extends RobotWithSensors {
	protected StateManager stateManager;
	
	public void setStateManager(StateManager manager) { this.stateManager = manager; }
	public StateManager getStateManager() { return this.stateManager; }
	
	public void tick() {
		stateManager.execute();
	}

}
