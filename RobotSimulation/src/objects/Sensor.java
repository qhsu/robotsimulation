package objects;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

import abstracts.BasicObject;
import interfaces.Detectable;
import interfaces.Detector;

/**
 * This class represents a sensor type that the robot has
 * 
 * @author Quinn
 *
 */
public class Sensor extends BasicObject implements Detector {
	protected int length;
	protected double angle;
	
	public Sensor (Point p, int length, double angle) {
		setDescription("sensor");
		setLocation(p);
		setDimension(new Dimension(length,length));
		setShape(new Line2D.Double(new Point(0,0), new Point2D.Double(length*Math.cos(angle), length*Math.sin(angle))));
		this.length = length;
		this.angle = angle;
	}

	@Override
	public void draw(Graphics g) {
		//Line2D line = recalculateLine();
		Shape line = getShape();
		Graphics2D g2d = (Graphics2D)g;
		g2d.draw(line);
		g.setColor(Color.black);
		g2d.draw(line.getBounds());

	}

	@Override
	public boolean detects(Detectable d) {
		Shape line = getShape();
		return line.intersects(d.getBounds()) || line.contains(d.getBounds()) || d.getBounds().contains(line.getBounds()) || d.getBounds().intersects(line.getBounds());
	}
}
