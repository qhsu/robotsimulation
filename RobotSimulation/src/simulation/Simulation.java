package simulation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import abstracts.BasicObject;
import factories.BoxFactory;
import factories.RobotFactory;
import objects.Box;

/**
 * The main class for running the simulation
 * @author Quinn
 *
 */

public class Simulation {
	
	@SuppressWarnings("serial")
	private static class MyPanel extends JPanel {
		
		private List<BasicObject> objects;
		private Box b = BoxFactory.getInstance().buildDefaultBox("Box", new Point(130,100), new Dimension(200,100));
		//private Box b2 = new Box("Box b2", new Point(254,100), new Dimension(200,100));
		//private Robot r1 = new Robot("r1", new Point(50,50), new Dimension(50,50));
		//private Robot r2 = RobotFactory.getInstance().buildBasicRobot("r2", new Point(100,100), new Dimension(25,25), 30);
				
		private Timer t;
		private boolean timerIsScheduled;
        
		
		public MyPanel () {
			setBorder(BorderFactory.createLineBorder(Color.black));
			
			objects = new ArrayList<BasicObject>();
			//objects.add(RobotFactory.getInstance().buildBasicRobot("r1", new Point(50,50), new Dimension(50,50), 10));
			objects.add(RobotFactory.getInstance().buildBasicRobot("r2", new Point(100,100), new Dimension(25,25), 30));
			
			t = new Timer(true);
			timerIsScheduled = false;
		}
		
		public Dimension getPreferredSize() {
			return new Dimension(800, 600);
		}
		
		
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			
			g.setColor(Color.red);
			g.fillRect(0, 0, 800, 600);
			
			g.setColor(Color.yellow);
			g.drawString("Custom panel", 10, 20);
			
			g.setColor(Color.blue);
			b.draw(g);
			g.setColor(Color.green);
			//b2.draw(g);
			g.setColor(Color.pink);
			//r1.draw(g);
			
			g.setColor(Color.orange);
			//r2.draw(g);
			for(BasicObject o: objects) {
				o.draw(g);
			}
			
			
		}
		
		public void start() {
			// At this point, if the timer has been cancelled because the stop button
			// has been pressed, we need to catch the illegal state exception and recreate
			// the timer object.
			//
			// we use the timerIsScheduled boolean to know whether a scheduled task is already
			// tasked to the Timer object because you could press the start button twice and have
			// the object move twice as fast.  
			
			try {
				if (!timerIsScheduled) {
					timerIsScheduled = true;
					t.scheduleAtFixedRate(new TimerTask() {
						public void run() {
							tick();
						}
					}, 0, 1*1000);
				}
			} catch (IllegalStateException ex) {
				t = new Timer(true);
				timerIsScheduled = false;
				start();
			}
		}
		
		public void stop() {
			t.cancel();
			timerIsScheduled = false;
		}
		
		private void tick() {
			for(BasicObject r: objects) {
				r.tick();
			}
			repaint();
		}
	}
	
	@SuppressWarnings("serial")
	private static class ButtonPanel extends JPanel {
		private JButton start;
		private JButton stop;
		
		public ButtonPanel(Simulation.MyPanel p) {
			start = new JButton("Start");
			stop = new JButton("Stop");
			this.setLayout(new GridLayout(0,2));
			
			this.add(start);
			this.add(stop);
			
			start.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					p.start();
				}
			});
			
			stop.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					p.stop();
					
				}
			});
		}
		
	}
	
	private static void createAndShowGUI() {
		JFrame f = new JFrame("Tester Frame");
		Simulation.MyPanel p = new Simulation.MyPanel();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
		f.add(new Simulation.ButtonPanel(p), BorderLayout.PAGE_START);
		f.add(p, BorderLayout.CENTER);
        f.pack();
        
        f.setVisible(true);
        
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
		

	}
	
	

}
